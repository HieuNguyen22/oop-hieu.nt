import '../dao/Database.dart';
import '../entity/Accessortion.dart';
import '../entity/Category.dart';
import '../entity/Product.dart';

class DatabaseDemo {

  final Database _database = Database.getInstant();

  DatabaseDemo();

  // Import data
  void initDatabase() {
    _database.insertTable('ProductTable', Product(1, "Product1", 3));
    _database.insertTable('ProductTable', Product(2, "Product2", 2));
    _database.insertTable('ProductTable', Product(3, "Product3", 1));
    _database.insertTable('ProductTable', Product(4, "Product4", 4));
    _database.insertTable('ProductTable', Product(5, "Product5", 5));
    _database.insertTable('ProductTable', Product(6, "Product6", 6));
    _database.insertTable('ProductTable', Product(7, "Product7", 10));
    _database.insertTable('ProductTable', Product(8, "Product8", 8));
    _database.insertTable('ProductTable', Product(9, "Product9", 9));
    _database.insertTable('ProductTable', Product(10, "Product10", 7));

    _database.insertTable('CategoryTable', Category(1, "Category1"));
    _database.insertTable('CategoryTable', Category(2, "Category2"));
    _database.insertTable('CategoryTable', Category(3, "Category3"));
    _database.insertTable('CategoryTable', Category(4, "Category4"));
    _database.insertTable('CategoryTable', Category(5, "Category5"));
    _database.insertTable('CategoryTable', Category(6, "Category6"));
    _database.insertTable('CategoryTable', Category(7, "Category7"));
    _database.insertTable('CategoryTable', Category(8, "Category8"));
    _database.insertTable('CategoryTable', Category(9, "Category9"));
    _database.insertTable('CategoryTable', Category(10, "Category10"));

    _database.insertTable('AccessoryTable', Accessortion(1, "Accessortion1"));
    _database.insertTable('AccessoryTable', Accessortion(2, "Accessortion2"));
    _database.insertTable('AccessoryTable', Accessortion(3, "Accessortion3"));
    _database.insertTable('AccessoryTable', Accessortion(4, "Accessortion4"));
    _database.insertTable('AccessoryTable', Accessortion(5, "Accessortion5"));
    _database.insertTable('AccessoryTable', Accessortion(6, "Accessortion6"));
    _database.insertTable('AccessoryTable', Accessortion(7, "Accessortion7"));
    _database.insertTable('AccessoryTable', Accessortion(8, "Accessortion8"));
    _database.insertTable('AccessoryTable', Accessortion(9, "Accessortion9"));
    _database.insertTable('AccessoryTable', Accessortion(10, "Accessortion10"));
  }

  //  Test insert
  void insertTableTest() {
    print("-- Insert");

    _database.insertTable('ProductTable', Product(99999, 'TEST INSERT', 99999));
    _database.insertTable('CategoryTable', Category(99999, 'TEST INSERT'));
    _database.insertTable('AccessoryTable', Accessortion(99999, 'TEST INSERT'));
    print("- After inserting");
    _database.printDB();
  }

  // Test select
  void selectTableTest() {
    print("-- Select");
    dynamic selectedList = _database.selectTable("ProductTable");
    for (dynamic item in selectedList) {
      item.displayInfo();
    }
    print("");
  }

  // Test update
  void updateTableTest() {
    print("-- Update");
    _database.updateTable("ProductTable", Product(1, 'TEST UPDATE', 999));
    _database.updateTable("CategoryTable", Category(1, 'TEST UPDATE'));
    _database.updateTable("AccessoryTable", Accessortion(1, 'TEST UPDATE'));
    _database.printDB();
    print('\n');
  }

  // Test update by id
  void updateTableByIDTest() {
    print("-- Update by ID");
    _database.updateTableByID(10, Product(10, 'TEST UPDATE BY ID', 999));
    _database.updateTableByID(10, Category(10, 'TEST UPDATE BY ID'));
    _database.updateTableByID(10, Accessortion(10, 'TEST UPDATE BY ID'));
    _database.printDB();
    print('\n');
  }

  // Test delete
  void deleteTableTest() {
    print("-- Delete");
    _database.deleteTable("ProductTable", Product(1, 'TEST UPDATE', 999));
    _database.deleteTable("CategoryTable", Category(1, 'TEST UPDATE'));
    _database.deleteTable("AccessoryTable", Accessortion(1, 'TEST UPDATE'));
    _database.printDB();
    print("\n");
  }

  // Test truncate
  void truncateTableTest() {
    print("-- Truncate");
    _database.truncateTable("ProductTable");
    _database.truncateTable("CategoryTable");
    _database.truncateTable("AccessoryTable");
    _database.printDB();
    print("\n");
  }

  // Test print table
  void printTableTest() {
    _database.printDB();
  }
}
