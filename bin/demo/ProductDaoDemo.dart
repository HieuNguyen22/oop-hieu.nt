import '../entity/Product.dart';
import '../dao/ProductDAO.dart';

class ProductDaoDemo {
  final ProductDAO _productDAO = ProductDAO();

  // Import data
  void initProductDAO() {
    _productDAO.insertDAO(Product(1, "Product1", 2));
    _productDAO.insertDAO(Product(2, "Product2", 4));
    _productDAO.insertDAO(Product(3, "Product3", 12));
    _productDAO.insertDAO(Product(4, "Product4", 1));
    _productDAO.insertDAO(Product(5, "Product5", 9));
    _productDAO.insertDAO(Product(6, "Product6", 3));
    _productDAO.insertDAO(Product(7, "Product7", 10));
    _productDAO.insertDAO(Product(8, "Product8", 7));
    _productDAO.insertDAO(Product(9, "Product9", 5));
    _productDAO.insertDAO(Product(10, "Product10", 11));
    _productDAO.insertDAO(Product(11, "Product11", 6));
    _productDAO.insertDAO(Product(12, "Product12", 8));
    _productDAO.insertDAO(Product(13, "Product13", 13));
    _productDAO.insertDAO(Product(14, "Product14", 7));
    _productDAO.insertDAO(Product(15, "Product15", 11));
    _productDAO.insertDAO(Product(16, "Product16", 3));
    _productDAO.insertDAO(Product(17, "Product17", 9));
  }

  //  Test insert
  void insertDaoTest() {
    print("-- Insert DAO");
    print(
        'Insert thanh cong?   ${_productDAO.insertDAO(Product(99999, 'TEST INSERT', 99999))}');
    print('\n');
  }

  // Test update
  void updateDaoTest() {
    print("-- Update DAO");
    print(
        'Update thanh cong?   ${_productDAO.updateDAO(Product(7, 'TEST UPDATE', 7))}');
    print('\n');
  }

  // Test delete
  void deleteDaoTest() {
    print("-- Delete DAO");
    print(
        'Delete thanh cong?   ${_productDAO.deleteDAO(Product(1, 'TEST UPDATE', 10))}');
    print('\n');
  }

  // Test find all
  void findAllDaoTest() {
    print("-- Find all");
    List returnedList = _productDAO.findAll('ProductTable');
    for (Product product in returnedList) {
      product.displayInfo();
    }
    print('\n');
  }

  // Test find by id
  void findByIdDaoTest(int id) {
    print("-- Find by id");
    Product returnedObject = _productDAO.findByID('ProductTable', id);
    returnedObject.displayInfo();
    print('\n');
  }

  // Test find by name
  void findByNameDaoTest(String name) {
    print("-- Find by name");
    Product returnedObject = _productDAO.findByName(name);
    returnedObject.displayInfo();
    print('\n');
  }
}
