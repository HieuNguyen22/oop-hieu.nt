import '../dao/CategoryDAO.dart';
import '../entity/Category.dart';

class CategoryDaoDemo {
  final CategoryDAO _categoryDAO = CategoryDAO();

  // Import data
  void initCategoryDAO() {
    _categoryDAO.insertDAO(Category(1, "Category1"));
    _categoryDAO.insertDAO(Category(2, "Category2"));
    _categoryDAO.insertDAO(Category(3, "Category3"));
    _categoryDAO.insertDAO(Category(4, "Category4"));
    _categoryDAO.insertDAO(Category(5, "Category5"));
    _categoryDAO.insertDAO(Category(6, "Category6"));
    _categoryDAO.insertDAO(Category(7, "Category7"));
    _categoryDAO.insertDAO(Category(8, "Category8"));
    _categoryDAO.insertDAO(Category(9, "Category9"));
    _categoryDAO.insertDAO(Category(10, "Category10"));
    _categoryDAO.insertDAO(Category(11, "Category11"));
    _categoryDAO.insertDAO(Category(12, "Category12"));
    _categoryDAO.insertDAO(Category(13, "Category13"));
  }

  //  Test insert
  void insertDaoTest() {
    print("-- Insert DAO");
    print(
        'Insert thanh cong?   ${_categoryDAO.insertDAO(Category(99999, 'TEST INSERT'))}');
    print('\n');
  }

  // Test update
  void updateDaoTest() {
    print("-- Update DAO");
    print(
        'Update thanh cong?   ${_categoryDAO.updateDAO(Category(10, 'TEST UPDATE'))}');
    print('\n');
  }

  // Test delete
  void deleteDaoTest() {
    print("-- Delete DAO");
    print(
        'Delete thanh cong?   ${_categoryDAO.deleteDAO(Category(1, 'TEST UPDATE'))}');
    print('\n');
  }

  // Test find all
  void findAllDaoTest() {
    print("-- Find all");
    List returnedList = _categoryDAO.findAll('CategoryTable');
    for (Category category in returnedList) {
      category.displayInfo();
    }
    print('\n');
  }

  // Test find by id
  void findByIdDaoTest(int id) {
    print("-- Find by id");
    Category returnedObject = _categoryDAO.findByID('CategoryTable', id);
    returnedObject.displayInfo();
    print('\n');
  }
}
