import '../dao/AccessoryDAO.dart';
import '../entity/Accessortion.dart';

class AccessoryDaoDemo {
  final AccessoryDAO _accessoryDAO = AccessoryDAO();

  /// Import data
  void initAccessoryDAO() {
    _accessoryDAO.insertDAO(Accessortion(1, "Category1"));
    _accessoryDAO.insertDAO(Accessortion(2, "Category2"));
    _accessoryDAO.insertDAO(Accessortion(3, "Category3"));
    _accessoryDAO.insertDAO(Accessortion(4, "Category4"));
    _accessoryDAO.insertDAO(Accessortion(5, "Category5"));
    _accessoryDAO.insertDAO(Accessortion(6, "Category6"));
    _accessoryDAO.insertDAO(Accessortion(7, "Category7"));
    _accessoryDAO.insertDAO(Accessortion(8, "Category8"));
    _accessoryDAO.insertDAO(Accessortion(9, "Category9"));
    _accessoryDAO.insertDAO(Accessortion(10, "Category10"));
    _accessoryDAO.insertDAO(Accessortion(11, "Category11"));
    _accessoryDAO.insertDAO(Accessortion(12, "Category12"));
    _accessoryDAO.insertDAO(Accessortion(13, "Category13"));
    _accessoryDAO.insertDAO(Accessortion(14, "Category14"));
    _accessoryDAO.insertDAO(Accessortion(15, "Category15"));
  }

  //  Test insert
  void insertDaoTest() {
    print("-- Insert DAO");
    print(
        'Insert thanh cong?   ${_accessoryDAO.insertDAO(Accessortion(99999, 'TEST INSERT'))}');
    print('\n');
  }

  // Test update
  void updateDaoTest() {
    print("-- Update DAO");
    print(
        'Update thanh cong?   ${_accessoryDAO.updateDAO(Accessortion(10, 'TEST UPDATE'))}');
    print('\n');
  }

  // Test delete
  void deleteDaoTest() {
    print("-- Delete DAO");
    print(
        'Delete thanh cong?   ${_accessoryDAO.deleteDAO(Accessortion(1, 'TEST UPDATE'))}');
    print('\n');
  }

  // Test find all
  void findAllDaoTest() {
    print("-- Find all");
    List returnedList = _accessoryDAO.findAll('AccessoryTable');
    for (Accessortion accessory in returnedList) {
      accessory.displayInfo();
    }
    print('\n');
  }

  // Test find by id
  void findByIdDaoTest(int id) {
    print("-- Find by id");
    Accessortion returnedObject = _accessoryDAO.findByID('AccessoryTable', id);
    returnedObject.displayInfo();
    print('\n');
  }
}
