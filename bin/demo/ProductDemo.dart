import '../entity/Product.dart';

class ProductDemo{

  ProductDemo();

  // Test create product
  void createProductTest(){
    Product product = Product(1, 'Laptop', 22);
    printProduct(product);
  }

  // Test print product
  void printProduct(Product product){
    print('-- Created product:');
    product.displayInfo();
  }
}