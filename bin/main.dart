import 'package:ex1_oop/ex1_oop.dart' as ex1_oop;

import 'dao/CategoryDAO.dart';
import 'dao/Database.dart';
import 'demo/AccessoryDaoDemo.dart';
import 'demo/ProductDaoDemo.dart';
import 'demo/CategoryDaoDemo.dart';
import 'demo/DatabaseDemo.dart';
import 'demo/ProductDemo.dart';
import 'entity/Accessortion.dart';
import 'entity/Category.dart';
import 'entity/Product.dart';

List<Product> pList = [];
List<Category> cList = [];
List<Accessortion> aList = [];
void main(List<String> arguments) {
  // TEST CREATE PRODUCT
  print("---- TEST CREATE PRODUCT");
  ProductDemo productDemo = ProductDemo();
  productDemo.createProductTest();
  print("");

  //  TEST DEMO DATABASE
  print("---- TEST DEMO DATABASE");
  DatabaseDemo databaseDemo = DatabaseDemo();

  // Check initDatabase()
  print('-- Import data');
  databaseDemo.initDatabase();
  databaseDemo.printTableTest();
  // Check insertTable()
  databaseDemo.insertTableTest();
  // Check selectTable()
  databaseDemo.selectTableTest();
  // Check updateTable()
  databaseDemo.updateTableTest();
  // Check updateTableByID()
  databaseDemo.updateTableByIDTest();
  // Check deleteTable()
  databaseDemo.deleteTableTest();
  // Check truncateTable()
  databaseDemo.truncateTableTest();
  print("");

  // TEST DEMO CATEGORY_DAO
  print("-- TEST DEMO CATEGORY_DAO");
  CategoryDaoDemo categoryDaoDemo = CategoryDaoDemo();

  // Check initCategoryDAO()
  categoryDaoDemo.initCategoryDAO();
  categoryDaoDemo.findAllDaoTest();
  // Check insertDaoTest()
  categoryDaoDemo.insertDaoTest();
  // Check updateDaoTest()
  categoryDaoDemo.updateDaoTest();
  // Check deleteDaoTest()
  categoryDaoDemo.deleteDaoTest();
  // Check findAllDaoTest()
  categoryDaoDemo.findAllDaoTest();
  // Check findByIdDaoTest()
  categoryDaoDemo.findByIdDaoTest(7);

  // TEST DEMO PRODUCT DAO
  print("-- TEST DEMO PRODUCT DAO");
  ProductDaoDemo productDaoDemo = ProductDaoDemo();

  // Check initCategoryDAO()
  productDaoDemo.initProductDAO();
  productDaoDemo.findAllDaoTest();
  // Check insertDaoTest()
  productDaoDemo.insertDaoTest();
  // Check updateDaoTest()
  productDaoDemo.updateDaoTest();
  // Check deleteDaoTest()
  productDaoDemo.deleteDaoTest();
  // Check findAllDaoTest()
  productDaoDemo.findAllDaoTest();
  // Check findByIdDaoTest()
  productDaoDemo.findByIdDaoTest(7);
  // Check findByNameDaoTest()
  productDaoDemo.findByNameDaoTest('Product2');

  // TEST DEMO ACCESSORY DAO
  print("-- TEST DEMO ACCESSORY DAO");
  AccessoryDaoDemo accessoryDaoDemo = AccessoryDaoDemo();

  // Check initCategoryDAO()
  accessoryDaoDemo.initAccessoryDAO();
  accessoryDaoDemo.findAllDaoTest();
  // Check insertDaoTest()
  accessoryDaoDemo.insertDaoTest();
  // Check updateDaoTest()
  accessoryDaoDemo.updateDaoTest();
  // Check deleteDaoTest()
  accessoryDaoDemo.deleteDaoTest();
  // Check findAllDaoTest()
  accessoryDaoDemo.findAllDaoTest();
  // Check findByIdDaoTest()
  accessoryDaoDemo.findByIdDaoTest(7);

  // TEST SINGLETON
  var database1 = Database();
  var database2 = Database();

  // So sanh dia chi
  print(
      '2 instants cua Database co cung dia chi khong? - ${identical(database1, database2)}');
}
