import 'Database.dart';

abstract class BaseDao {
  Database database = Database.getInstant();

  /// Insert data
  ///
  /// Call the funciton [insertTable] in class Database
  /// with param [row]
  int insertDAO(dynamic row) {
    return database.insertTable(row.getNameEntity, row);
  }

  /// Update data
  ///
  /// Call the funciton [updateTable] in class Database
  /// with param [row]
  int updateDAO(dynamic row) {
    return database.updateTable(row.getNameEntity, row);
  }

  /// Delete data
  ///
  /// Call the funciton [deleteTable] in class Database
  /// with param [row]
  int deleteDAO(dynamic row) {
    return database.deleteTable(row.getNameEntity, row);
  }

  /// Get all table data
  ///
  /// Call the function [getTable] in class Database
  /// with [name] of the table
  List findAll(String name) {
    return database.getTable(name);
  }

  /// Get object of table by ID
  ///
  /// Call the function [findObjectByID] in class Database
  /// with [name] of the table nad [id] of the object
  dynamic findByID(String name, int id) {
    return database.findObjectByID(name, id);
  }
}
