import '../entity/Product.dart';
import 'BaseDao.dart';

class ProductDAO extends BaseDao {

  /// Find product by name
  ///
  /// Call the funciton [findObjectByName] in class Database
  /// with param [nameProduct]
  Product findByName(String nameProduct) {
    return database.findObjectByName('ProductTable', nameProduct);
  }
}
