// import 'dart:indexed_db';

import '../entity/Accessortion.dart';
import '../entity/Category.dart';
import '../entity/Product.dart';

class Database {
  List<Product> _productTable = [];
  List<Category> _categoryTable = [];
  List<Accessortion> _accessoryTable = [];

  static final Database _instant = Database._internal();

  static final String PRODUCT = 'ProductTable';
  static final String CATEGORY = 'CategoryTable';
  static final String ACCESSORY = 'AccessoryTable';

  // Build Singleton by using keywork 'factory'
  factory Database() {
    return _instant;
  }

  // Private constructor
  Database._internal();

  // Don't use anymore
  static Database getInstant() {
    return _instant;
  }

  /// Insert data
  /// 
  /// Insert data to the table with the [name] of the table and [row].
  /// Return 1 if success, 0 if fail
  int insertTable(String name, dynamic row) {
    if (name.compareTo(PRODUCT) == 0) {
      _productTable.add(row);
      return 1;
    } else if (name.compareTo(CATEGORY) == 0) {
      _categoryTable.add(row);
      return 1;
    } else if (name.compareTo(ACCESSORY) == 0) {
      _accessoryTable.add(row);
      return 1;
    }
    return 0;
  }

  /// Select data
  ///
  /// Get data with the [name] of the table. Return list data
  dynamic selectTable(String name) {
    if (name.compareTo(PRODUCT) == 0) {
      return _productTable;
    } else if (name.compareTo(CATEGORY) == 0) {
      return _categoryTable;
    } else if (name.compareTo(ACCESSORY) == 0) {
      return _accessoryTable;
    } else {
      return null;
    }
  }

  /// Update
  /// 
  /// Update data with the [name] of the table and [row].
  /// Return 1 if success, 0 if fail
  int updateTable(String name, dynamic row) {
    int updateRowID = row.getID;
    if (name.compareTo(PRODUCT) == 0) {
      for (Product product in _productTable) {
        if (product.getID == updateRowID) {
          _productTable[updateRowID - 1] = row;
          break;
        }
      }
      return 1;
    } else if (name.compareTo(CATEGORY) == 0) {
      for (Category category in _categoryTable) {
        if (category.getID == updateRowID) {
          _categoryTable[updateRowID - 1] = row;
          break;
        }
      }
      return 1;
    } else if (name.compareTo(ACCESSORY) == 0) {
      for (Accessortion accessory in _accessoryTable) {
        if (accessory.getID == updateRowID) {
          _accessoryTable[updateRowID - 1] = row;
          break;
        }
      }
      return 1;
    }
    return 0;
  }

  /// Update by ID
  /// 
  /// Update data with the [id] and [row].
  /// Return 1 if success, 0 if fail.
  int updateTableByID(int id, dynamic row) {
    String nameEntityRow = row.getNameEntity;

    if (nameEntityRow.compareTo(PRODUCT) == 0) {
      _productTable[id - 1] = row;
      return 1;
    } else if (nameEntityRow.compareTo(CATEGORY) == 0) {
      _categoryTable[id - 1] = row;
      return 1;
    } else if (nameEntityRow.compareTo(ACCESSORY) == 0) {
      _accessoryTable[id - 1] = row;
      return 1;
    }
    return 0;
  }

  /// Delete
  /// 
  /// Delete object with [name] of the table and [row].
  /// Return 1 if success, 0 if fail.
  int deleteTable(String name, dynamic row) {
    int updateRowID = row.getID;
    if (name.compareTo(PRODUCT) == 0) {
      for (Product product in _productTable) {
        if (product.getID == updateRowID) {
          _productTable.remove(_productTable[updateRowID - 1]);
          break;
        }
      }
      return 1;
    } else if (name.compareTo(CATEGORY) == 0) {
      for (Category category in _categoryTable) {
        if (category.getID == updateRowID) {
          _categoryTable.remove(_categoryTable[updateRowID - 1]);
          break;
        }
      }
      return 1;
    } else if (name.compareTo(ACCESSORY) == 0) {
      for (Accessortion accessory in _accessoryTable) {
        if (accessory.getID == updateRowID) {
          _accessoryTable.remove(_accessoryTable[updateRowID - 1]);
          break;
        }
      }
      return 1;
    }
    return 0;
  }

  /// Truncate
  /// 
  /// Truncate table with [name] of the table.
  /// Return 1 if success, 0 if fail.
  int truncateTable(String name) {
    if (name.compareTo(PRODUCT) == 0) {
      _productTable.clear();
      return 1;
    } else if (name.compareTo(CATEGORY) == 0) {
      _categoryTable.clear();
      return 1;
    } else if (name.compareTo(ACCESSORY) == 0) {
      _accessoryTable.clear();
      return 1;
    }
    return 0;
  }

  /// Print all table in database
  void printDB() {
    print("Product Table: ");
    printTable(PRODUCT);
    print("");

    print("Category Table: ");
    printTable(CATEGORY);
    print("");

    print("Accessory Table: ");
    printTable(ACCESSORY);
    print("");
  }

  /// Print table
  /// 
  /// Print table with its [name]
  void printTable(String name) {
    if (name.compareTo(PRODUCT) == 0) {
      for (Product product in _productTable) {
        product.displayInfo();
      }
    } else if (name.compareTo(CATEGORY) == 0) {
      for (Category category in _categoryTable) {
        category.displayInfo();
      }
    } else if (name.compareTo(ACCESSORY) == 0) {
      for (Accessortion accessory in _accessoryTable) {
        accessory.displayInfo();
      }
    }
  }

  /// Get table
  /// 
  /// Get table with its [name]. Return list data
  List<dynamic> getTable(String name) {
    if (name.compareTo(PRODUCT) == 0) {
      return _productTable;
    } else if (name.compareTo(CATEGORY) == 0) {
      return _categoryTable;
    } else {
      return _accessoryTable;
    }
  }

  /// Find objecy by ID
  /// 
  /// Get object with [name] of table and [id]. 
  /// Return object
  dynamic findObjectByID(String name, int id) {
    if (name.compareTo(PRODUCT) == 0) {
      for (Product product in _productTable) {
        if (product.getID == id) {
          return product;
        }
      }
    } else if (name.compareTo(CATEGORY) == 0) {
      for (Category category in _categoryTable) {
        if (category.getID == id) {
          return category;
        }
      }
    } else if (name.compareTo(ACCESSORY) == 0) {
      for (Accessortion accessory in _accessoryTable) {
        if (accessory.getID == id) {
          return accessory;
        }
      }
    }

    return null;
  }

  /// Find objecy by name
  /// 
  /// Get object with [nameTable] and [nameObject]. 
  /// Return object
  dynamic findObjectByName(String nameTable, String nameObject) {
    if (nameTable.compareTo(PRODUCT) == 0) {
      for (Product product in _productTable) {
        if (product.getName == nameObject) {
          return product;
        }
      }
    } else if (nameTable.compareTo(CATEGORY) == 0) {
      for (Category category in _categoryTable) {
        if (category.getName == nameObject) {
          return category;
        }
      }
    } else if (nameTable.compareTo(ACCESSORY) == 0) {
      for (Accessortion accessory in _accessoryTable) {
        if (accessory.getName == nameObject) {
          return accessory;
        }
      }
    }

    return null;
  }

}
