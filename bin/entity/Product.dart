import 'BaseRow.dart';

class Product extends BaseRow{
  int? _categoryID;

  /// Set the constructor with [id], [name] and [categoryID]
  Product(int id, String name, int categoryID){
    this.id = id;
    this.name = name;
    this._categoryID = categoryID;
  }

  /// Get cate ID
  /// 
  /// Return [_categoryID]
  int? get getCateID {
    return _categoryID;
  }

  // Set Category ID
  ///
  /// Set [_categoryID] of the object by [cateID]
  set setCateID(int cateID) {
    _categoryID = cateID;
  }

  /// Get name of the Entity of the object
  @override
  String get getNameEntity {
    return 'ProductTable';
  }

  @override
  void displayInfo() {
    print("${super.getID} - ${super.getName} - $_categoryID");
  }
}
