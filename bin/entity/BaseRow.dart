import 'IENtity.dart';

abstract class BaseRow{
  int? id;
  String? name;

  /// Get id of object
  /// 
  /// Return the [id]
  int? get getID {
    return id;
  }


  /// Set id of object
  /// 
  /// Set the [id] of the object by [inputID]
  set setID(int inputID) {
    id = inputID;
  }


  /// Get name of the object
  /// 
  /// Return the [name] of the object
  String? get getName {
    return name;
  }


  /// Set name of object
  /// 
  /// Set the [name] of the object by [inputName]
  set setName(String inputName) {
    name = inputName;
  }


  // Abstract method
  String get getNameEntity;


  /// Show info of the object
  /// 
  /// Print the object's info with [id] and [name]
  void displayInfo() {
    print("$id - $name");
  }
}
