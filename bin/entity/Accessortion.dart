import 'BaseRow.dart';

class Accessortion extends BaseRow{

  /// Set the constructor with [id] and [name]
  Accessortion(int id, String name){
    this.id = id;
    this.name = name;
  }

  /// Get name of the Entity of the object
  @override
  String get getNameEntity {
    return 'AccessoryTable';
  }

}
